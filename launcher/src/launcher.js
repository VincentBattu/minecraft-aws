const {CloudFormation, Credentials, Lambda} = require('aws-sdk')
const crypto = require('crypto')
const { clipboard } = require('electron')

const ramParameters = {
  't2.micro': [{ ParameterKey: 'MinRam', ParameterValue: '256m' }, { ParameterKey: 'MaxRam', ParameterValue: '512m' }],
  't3.small': [{ ParameterKey: 'MinRam', ParameterValue: '1024m' }, { ParameterKey: 'MaxRam', ParameterValue: '1512m' }],
  't3a.medium': [{ ParameterKey: 'MinRam', ParameterValue: '2048m' }, { ParameterKey: 'MaxRam', ParameterValue: '3512m' }],
  't3a.large': [{ ParameterKey: 'MinRam', ParameterValue: '2048m' }, { ParameterKey: 'MaxRam', ParameterValue: '7512m' }],
  't3a.xlarge': [{ ParameterKey: 'MinRam', ParameterValue: '2048m' }, { ParameterKey: 'MaxRam', ParameterValue: '15512m' }]
}

const accessKeyIdLocalstorageKey = 'ACCESS_KEY_ID'
const secretAccessKeyLocalstorageKey = 'SECRET_ACCESS_KEY'
const accessKeyId = localStorage.getItem(accessKeyIdLocalstorageKey)
const secretAccessKey = localStorage.getItem(secretAccessKeyLocalstorageKey)
const $accessKeyId = document.querySelector('#access-key-id')
const $secretAccessKey = document.querySelector('#secret-access-key')


if (accessKeyId && secretAccessKey) {
  setAccessKeyId(accessKeyId)
  setSecretAccessKey(secretAccessKey)
}

updateStatus()

function saveAccessKeyInLocalstorage (e) {
  const accessKeyId = e.target.value
  localStorage.setItem(accessKeyIdLocalstorageKey, accessKeyId)
}
$accessKeyId.addEventListener('keyup',saveAccessKeyInLocalstorage)
$accessKeyId.addEventListener('paste',saveAccessKeyInLocalstorage)
$accessKeyId.addEventListener('focusout',saveAccessKeyInLocalstorage)

function saveSecretAccessKeyInLocalstorage (e) {
  const accessKeyId = e.target.value
  localStorage.setItem(secretAccessKeyLocalstorageKey, accessKeyId)
}
$secretAccessKey.addEventListener('keyup', saveSecretAccessKeyInLocalstorage)
$secretAccessKey.addEventListener('paste', saveSecretAccessKeyInLocalstorage)
$secretAccessKey.addEventListener('focusout', saveSecretAccessKeyInLocalstorage)

document.querySelector('#refresh-button').addEventListener('click', () => {
  updateStatus()
})

document.querySelector('#btn-copy-address').addEventListener('click', (e) => {
  clipboard.writeText(document.querySelector('#ip-address').innerHTML)
})

document.querySelector('#btn-start').addEventListener('click', async (e) => {
  e.preventDefault()
  disableButtons()
  displayLoader('start')
  const cloudFormation = new CloudFormation({
    region: 'eu-west-3',
    credentials: new Credentials(getAccessKeyId(), getSecretAccessKey())
  })
  const stacksResponse = await cloudFormation.describeStacks().promise()
  const stackAlreadyExists = stacksResponse.Stacks.some((stack) => stack.Tags.some((tag) => tag.Key === 'type' && tag.Value === 'minecraft'))
  if (stackAlreadyExists) {
    await updateStatus()
    hideLoader()
    return
  }
  const instanceSize = document.querySelector('#instance-size').value
  const stack = await cloudFormation.createStack({
    StackName: 'minecraft-' + crypto.randomFillSync(Buffer.alloc(5)).toString('hex'),
    TemplateURL: 'https://brohazard-cloud-formation-template.s3.eu-west-3.amazonaws.com/StartMinecraftServer.yaml',
    Tags: [
      { Key: 'type', Value: 'minecraft' }
    ],
    Parameters: [
      { ParameterKey: 'InstanceTypeParameter', ParameterValue: instanceSize},
      ...ramParameters[instanceSize],
      { ParameterKey: 'DataBucket', ParameterValue: 'brohazard-minecraft-data' },
    ],
  }).promise()
  await cloudFormation.waitFor('stackCreateComplete', {
    StackName: stack.StackId
  }).promise()
  const {publicIp} = await updateStatus()
  const notification = new Notification('Serveur Démarré', {
    body: `Port: ${publicIp} \n(Cliquer pour copier l'ip)`
  })
  notification.addEventListener('click', () => {
    clipboard.writeText(publicIp)
  })
  hideLoader()
})

document.querySelector('#btn-stop').addEventListener('click', async (e) => {
  e.preventDefault()
  disableButtons()
  displayLoader('stop')
  const lambda = new Lambda({
    region: 'eu-west-3',
    credentials: new Credentials(getAccessKeyId(), getSecretAccessKey())
  })
  const invoked = await lambda.invoke({
    FunctionName: 'StopMinecraftServer',
    Payload: JSON.stringify({
      outputS3DataBucket: 'brohazard-minecraft-data'
    })
  }).promise()
  const cloudFormation = new CloudFormation({
    region: 'eu-west-3',
    credentials: new Credentials(getAccessKeyId(), getSecretAccessKey())
  })
  const stacksResponse = await cloudFormation.describeStacks().promise()
  const stack = stacksResponse.Stacks.find((stack) => stack.Tags.some((tag) => tag.Key === 'type' && tag.Value === 'minecraft'))
  if (!stack) {
    await updateStatus()
    hideLoader()
    return
  }
  await cloudFormation.waitFor('stackDeleteComplete', {
    StackName: stack.StackId
  }).promise()
  await updateStatus()
  new Notification('Server Arrêté', {
    body: ''
  })
  hideLoader()
})


/**
 * @return {Promise<{publicIp: string}>}
 */
async function updateStatus() {
  const accessKeyId = getAccessKeyId()
  const secretAccessKey = getSecretAccessKey()
  if (!accessKeyId || !secretAccessKey) return
  const cloudFormation = new CloudFormation({
    region: 'eu-west-3',
    credentials: new Credentials(accessKeyId, secretAccessKey)
  })
  const stacksResponse = await cloudFormation.describeStacks().promise()
  const stack = stacksResponse.Stacks.find((stack) => stack.Tags.some((tag) => tag.Key === 'type' && tag.Value === 'minecraft'))
  if (stack) {
    const instanceSize = stack.Parameters.find((parameter) => parameter.ParameterKey === 'InstanceTypeParameter')?.ParameterValue
    setInstanceSize(instanceSize)
    const publicIp = stack.Outputs.find((output) => output.OutputKey === 'PublicIp')?.OutputValue
    setIpAddress(publicIp)
    document.querySelector('#btn-start').disabled = true
    document.querySelector('#btn-stop').disabled = false
    document.querySelector('#btn-copy-address').disabled = false
    document.querySelector('#server-status-started').style.display = 'inline'
    document.querySelector('#server-status-stopped').style.display = 'none'
    return {publicIp}
  } else {
    setIpAddress('')
    document.querySelector('#btn-start').disabled = false
    document.querySelector('#btn-stop').disabled = true
    document.querySelector('#btn-copy-address').disabled = true
    document.querySelector('#server-status-started').style.display = 'none'
    document.querySelector('#server-status-stopped').style.display = 'inline'
    return {publicIp: ''}
  }
}

const counterToReach = Math.floor(Math.random() * 100);
let currentCount = 0
tryEnableEasterEgg()

document.querySelector('#server-img').addEventListener('click', (e) => {
  currentCount++
  tryEnableEasterEgg()
})

function tryEnableEasterEgg () {
  if (currentCount === counterToReach) {
    document.querySelector('#server-img').src = '../img/server-easter-egg.png'
  }
}

/**
 * @return {void}
 */
function disableButtons() {
  document.querySelector('#btn-start').disabled = true
  document.querySelector('#btn-stop').disabled = true
}

/**
 * @return {string}
 */
function getAccessKeyId() {
  return $accessKeyId.value
}

/**
 * @param {string} accessKeyId 
 * 
 * @returns {void}
 */
function setAccessKeyId (accessKeyId) {
  $accessKeyId.value = accessKeyId
}

/**
 * @return {string}
 */
function getSecretAccessKey() {
  return $secretAccessKey.value
}

/**
 * @param {string} secretAccessKey 
 * 
 * @returns {void}
 */
function setSecretAccessKey(secretAccessKey) {
  return $secretAccessKey.value = secretAccessKey
}

/**
 * @param {string} publicIp
 */
function setIpAddress(publicIp) {
  document.querySelector('#ip-address').innerHTML = publicIp
}

/**
 * @param {string} value
 */
function setInstanceSize(value) {
  document.querySelector('#instance-size').value = value
}

/**
 * @param {string} action
 */
function displayLoader(action) {
  document.querySelector('#loader').style.display = 'block'
  document.querySelector('#loader-message-' + action).style.display = 'inline'
}

/**
 * @return {void}
 */
function hideLoader() {
  document.querySelector('#loader').style.display = 'none'
  document.querySelector('#loader-message-start').style.display = 'none'
  document.querySelector('#loader-message-stop').style.display = 'none'
}