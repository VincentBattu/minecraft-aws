const { app, BrowserWindow } = require('electron')
const { resolve } = require('path')

const production = process.env.NODE_ENV === 'production'

function createWindow () {
  let win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      devTools: !production
    }
  })

  if (production) {
    win.setMenu(null)
  }

  win.loadFile(resolve(__dirname, '../public/view/index.html'))

  win = null
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
