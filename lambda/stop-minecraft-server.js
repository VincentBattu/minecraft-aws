const { SSM, CloudFormation } = require('aws-sdk')

/**
 * 
 * @param {object} event 
 * @param {string} event.outputS3DataBucket 
 */
exports.handler = async (event) => {
  if (!event.outputS3DataBucket) {
    throw new Error('Missing outputS3DataBucket parameter')
  }
  const ssm = new SSM()
  const cloudFormation = new CloudFormation()

  const stacks = await cloudFormation.describeStacks().promise()

  const minecraftStack = stacks.Stacks.find(stack => stack.Tags.some(tag => tag.Key === 'type' && tag.Value === 'minecraft'))

  const resources = await cloudFormation.describeStackResources({
    StackName: minecraftStack.StackId
  }).promise()

  const minecraftEc2Instance = resources.StackResources.find(resource => resource.ResourceType === 'AWS::EC2::Instance')

  if (minecraftEc2Instance) {
    await ssm.sendCommand({
      DocumentName: 'AWS-RunShellScript' ,
      Parameters: {
        commands: [
          "screen -X stuff 'stop^M'",
          'aws configure set region eu-west-3',
          `aws s3 sync /usr/games/minecraft s3://${event.outputS3DataBucket}`,
          `aws cloudformation delete-stack --stack-name ${minecraftStack.StackId}`
        ],
        executionTimeout: ['3600'],
      },
      InstanceIds: [minecraftEc2Instance.PhysicalResourceId]
    }).promise()
  }
}
